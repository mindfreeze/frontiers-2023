# HDR Metrics Optimisation

## The impact of HDR Metrics on Rate-Distortion Optimisation for AV1

Vibhoothi$^{\dagger}$, François Pitié$^{\dagger}$, Angeliki Katsenou$^{\dagger}$, Yeping Su$^{\ddagger}$, Balu Adsumilli$^{\ddagger}$, Anil Kokaram$^{\dagger}$

$^{\dagger}$ Sigmedia Group, Department of Electronic and Electrical
Engineering, *Trinity College Dublin*, Ireland

$^{\ddagger}$ YouTube Media-Algorithms Team, *Google Inc*, California, USA

$^{\dagger}$\{vibhoothi, pitief, akatsenou, anil.kokaram\}at tcd dot ie

$^{\ddagger}$ \{yeping,badsumilli\} at google dot com



## Table of Contents

[[_TOC_]]

## Abstract

In recent years, there has been a notable increase in the delivery of High-Dynamic Range (HDR) displays and associated content. In this work, we review the popular HDR metrics
DeltaE100 (DE100), PSNRL100, wPSNR,  HDR-VQM and SDR metrics PSNR, VMAF and MS-SSIM in the context of rate-distortion optimisation. Specifically, we compare the optimisation obtained when using these metrics in per-clip optimisation of the rate-distortion Lagrange multiplier in AV1. Results on 35 HDR videos indicate that parameters optimised for SDR metrics do, indeed, transfer well to HDR metrics, but also that there is room for improvement in HDR metrics as metrics that use chroma seem to be at odds with perceptual models operating on luma only. This also leads to the more general observation that we must be careful about how colour is handled in RDO optimisation. We show that the popular way of using BD-rate for luma-only quality metrics (\textit{eg.} PSNR, VMAF or MS-SSIM) as an objective function is, in fact, flawed as they negatively bias against chroma. We also show that the Chroma $qp$-Offsets established in HEVC are also optimal for AV1, and that they are effective in establishing a balance between chroma and luma.

## Dataset

We have used the Dataset from our previous work [1.][2.] which contains 50
4K HDR sequences, in this work we are using a subset of them @ 35, these can be
also represented as "Shot-group" as shown in the orignial paper. This is shown
below for reference.

<Details>

|#|Video|Video Set|Shot-group|Resolution |Framerate|
|:----|:----|:----|:----|:----|:----|
|1|aom_cosmos_3840x2160_24_hdr10_11589-11752.y4m|AOM-Candidates|Cosmos|1920x1080|24|
|2|aom_cosmos_3840x2160_24_hdr10_12916-13078.y4m|AOM-Candidates|Cosmos|1920x1080|24|
|3|aom_cosmos_3840x2160_24_hdr10_1573-1749.y4m|AOM-Candidates|Cosmos|1920x1080|24|
|4|aom_cosmos_3840x2160_24_hdr10_8686-8826.y4m|AOM-Candidates|Cosmos|1920x1080|24|
|5|aom_cosmos_3840x2160_24_hdr10_9561-9789.y4m|AOM-Candidates|Cosmos|1920x1080|24|
|6|aom_meridian_3840x2160_5994_hdr10_12264-12745.y4m|AOM-Candidates|Meridian |1920x1080|59.94|
|7|aom_meridian_3840x2160_5994_hdr10_15932-16309.y4m|AOM-Candidates|Meridian |1920x1080|59.94|
|8|aom_meridian_3840x2160_5994_hdr10_1782-2163.y4m|AOM-Candidates|Meridian |1920x1080|59.94|
|9|aom_meridian_3840x2160_5994_hdr10_22412-22738.y4m|AOM-Candidates|Meridian |1920x1080|59.94|
|10|aom_meridian_3840x2160_5994_hdr10_24058-24550.y4m|AOM-Candidates|Meridian |1920x1080|59.94|
|11|aom_nocturne_3840x2160_60_hdr10_17140-17709.y4m|AOM-Candidates|Nocturne|1920x1080|60|
|12|aom_nocturne_3840x2160_60_hdr10_27740-28109.y4m|AOM-Candidates|Nocturne|1920x1080|60|
|13|aom_nocturne_3840x2160_60_hdr10_32660-32799.y4m|AOM-Candidates|Nocturne|1920x1080|60|
|14|aom_nocturne_3840x2160_60_hdr10_9010-9349.y4m|AOM-Candidates|Nocturne|1920x1080|60|
|15|aom_sol_levante_3840x2160_24_hdr10_2268-2412.y4m|AOM-Candidates|Sol-Levante|1920x1080|24|
|16|aom_sol_levante_3840x2160_24_hdr10_289-453.y4m|AOM-Candidates|Sol-Levante|1920x1080|24|
|17|aom_sol_levante_3840x2160_24_hdr10_3282-3874.y4m|AOM-Candidates|Sol-Levante|1920x1080|24|
|18|aom_sol_levante_3840x2160_24_hdr10_4123-4545.y4m|AOM-Candidates|Sol-Levante|1920x1080|24|
|19|aom_sparks_4096x2160_5994_hdr10_11198-11570.y4m|AOM-Candidates|Sparks|1920x1080|59.94|
|20|aom_sparks_4096x2160_5994_hdr10_350-480.y4m|AOM-Candidates|Sparks|1920x1080|59.94|
|21|aom_sparks_4096x2160_5994_hdr10_6026-6502.y4m|AOM-Candidates|Sparks|1920x1080|59.94|
|22|aom_sparks_4096x2160_5994_hdr10_8396-8941.y4m|AOM-Candidates|Sparks|1920x1080|59.94|
|23|MeridianRoad_3840x2160_5994_hdr10.y4m|AOM-CTC|Meridian |1920x1080|59.94|
|24|NocturneDance_3840x2160_60fps_hdr10.y4m|AOM-CTC|Nocturne|1920x1080|60|
|25|NocturneRoom_3840x2160_60fps_hdr10.y4m|AOM-CTC|Nocturne|1920x1080|60|
|26|SparksWelding_4096x2160_5994_hdr10.y4m|AOM-CTC|Sparks|1920x1080|59.94|
|27|svt_midnight_sun_3840x2160_50_10bit_420p.y4m|svt-opencontent|SVT|1920x1080|50|
|28|svt_smithy_3840x2160_50_10bit_420p.y4m|svt-opencontent|SVT|1920x1080|50|
|29|svt_smoke_sauna_3840x2160_50_10bit_420p.y4m|svt-opencontent|SVT|1920x1080|50|
|30|svt_water_flyover_3840x2160_50_10bit_420p.y4m|svt-opencontent|SVT|1920x1080|50|
|31|svt_waterfall_3840x2160_50_10bit_420p.y4m|svt-opencontent|SVT|1920x1080|50|
|32|wipe_3840x2160_5994_10bit_420_1747-1877.y4m|Cables-4K|Wipe|1920x1080|59.94|
|33|wipe_3840x2160_5994_10bit_420_3857-3987.y4m|Cables-4K|Wipe|1920x1080|59.94|
|34|wipe_3840x2160_5994_10bit_420_8750-8880.y4m|Cables-4K|Wipe|1920x1080|59.94|
|35|wipe_3840x2160_5994_10bit_420_881-1011.y4m|Cables-4K|Wipe|1920x1080|59.94|


</Details>

Classification of shot groups:

| Shot Group                                                                                                         | Description                                                  |
| ------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------ |
| Cosmos<sup>[1](https://opencontent.netflix.com/)</sup>                                                             | Vibrant animated sequence, high temporal complexity, 24fps   |
| Meridian<sup>[1](https://opencontent.netflix.com/),[2](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)</sup>    | Natural sequence, high spatial complexity, 59.94fps          |
| Nocturne<sup>[1](https://opencontent.netflix.com/),[2](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)</sup>    | Natural sequence, medium spatial complexity, 60fps           |
| Sol Levante<sup>[1](https://opencontent.netflix.com/),[2](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)</sup> | Animated sequence, medium-high temporal complexity, 24fps    |
| Sparks<sup>[1](https://opencontent.netflix.com/),[2](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)</sup>      | Sequence with medium motion and wide dynamic range, 59.94fps |
| SVT<sup>[3](https://www.svt.se/open/en/content/)</sup>                                                             | Very high natural complexity sequences, 50fps                |
| Cables 4K<sup>[4](https://www.cablelabs.com/4k)</sup>                                                              | Outdoor sequences with moderate complexity, 59.94fps         |

Sample frames from the subset of the dataset which is tonemapped to BT.709 for
representation is shown below,

![Frame sequences](https://i.imgur.com/9tPPvEr.jpg)

> Note: Sequences are tonemapped to BT.709 for better representation,
```shell
ffmpeg -i $input.y4m -vf zscale=tin=smpte2084:min=bt2020nc:pin=bt2020:rin=tv:t=smpte2084:m=bt2020nc:p=bt2020:r=tv,zscale=t=linear:npl=100,format=gbrpf32le,zscale=p=bt709,tonemap=tonemap=hable:desat=0,zscale=t=bt709:m=bt709:r=tv,format=yuv420p png/test%04d.png
```

The source for these clips are:
1) [Netflix Open Content](https://opencontent.netflix.com/), also available at
[AOM-CTC](https://media.xiph.org/video/aomctc/test_set/hdr1_4k/),
[AOM-Candidates](https://media.xiph.org/video/av2/), [Netflix](http://download.opencontent.netflix.com.s3.amazonaws.com/index.html?prefix=aom_test_materials/HDR/).
1) [Cables 4K](https://www.cablelabs.com/4k)
2) [SVT Open Content](https://www.svt.se/open/en/content/)


## Encoder Configuration

libaom-av1-3.2.0, [287164d](https://aomedia.googlesource.com/aom/+/287164d)

Random-Access Preset as per [AOM-CTC](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)
``` sh
$AOMEMC --cpu-used=0 --passes=1 --lag-in-frames=19 --auto-alt-ref=1 --min-gf-interval=16 --max-gf-interval=16
--gf-min-pyr-height=4 --gf-max-pyr-height=4 --limit=130 --kf-min-dist=65 --kf-max-dist=65 --use-fixed-qp-offsets=1
--deltaq-mode=0 --enable-tpl-model=0 --end-usage=q --cq-level=$Q --enable-keyframe-filtering=0 --threads=1
--test-decode=fatal -o output.ivf --color-primaries=bt2020 --transfer-characteristics=smpte2084
--matrix-coefficients=bt2020ncl --chroma-sample-position=colocated $OPTIONS $INPUT.Y4M
```
> GOP-Size is fixed at 65, Sub-GOP is 16, Temporal-layers is 4, Closed-GOP
GOP-Size is set in AV1 with help of `kf-min-dist` and `kf-max-dist`. Sub-GOP
size is set in AV1 using `min-gf-interval` and `max-gf-interval`. The temporal
layers is defined using `gf-min-pyr-height` and `gf-max-pyr-height`. To have a
hierarchical reference structure, the number of temporal layers should be set to
$log2(SubGopsize)$. Open-GOP and Closed GOP is controlled with help of `
--enable-fwd-kf` inside AV1. Color Primaries, Transfer Charecteristics, Matrix
Coffecients are also specified which is required HDR Metadata.

All-Intra Preset as per [AOM-CTC](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf)
``` sh
$AOMENC --codec=av1 --cpu-used=0 --passes=1 --end-usage=q --cq-level=$x --kf-min-dist=0 --kf-max-dist=0 --use-fixed-qp-offsets=1 --deltaq-mode=0 --enable-tpl-model=0 --enable-keyframe-filtering=0 --ivf --threads=1 --test-decode=fatal -o $output.ivf --cpu-used=6 --color-primaries=bt2020 --transfer-characteristics=smpte2084 --matrix-coefficients=bt2020ncl --chroma-sample-position=colocated --bit-depth=10 $input.y4m
```
> All Intra configuration, with GOP and Sub-GOP length to be 0, Color Primaries, Transfer Charecteristics, Matrix
Coffecients, Bitdepth is signalled as HDR Metadata.


## Quantization Parameters(QP)

| SL-No | libaom-av1 |
| ----- | ---------- |
| 1     | 27         |
| 2     | 39         |
| 3     | 49         |
| 4     | 59         |
| 5     | 63         |

The QP points were chosen based on the [AOM-Common Testing Configuration(CTC.)](https://aomedia.org/docs/CWG-B075o_AV2_CTC_v2.pdf).



## *Encoder Modifications*
In, libaom-AV1, We have modifed the release version to support feeding
$`\lambda`$ values via command-line argument.

For applying Chroma and Luma Offsets, we applied ChangeID
[I99757226aea84d415f3fcf405ef3f4d90bdbf7dd](https://aomedia-review.googlesource.com/q/I99757226aea84d415f3fcf405ef3f4d90bdbf7dd)
on top of the release tag v3.2.0 which we are using to allow us to control
chroma offsets via command-line parameters.


## *Optimiser Setup*
For this experiment, we using Powell method as adopted by [1.], which is implemented in Scipy's optimise module
[scipy.optimise.minimise](https://docs.scipy.org/doc/scipy/reference/optimize.minimize-powell.html#optimize-minimize-powell).

## Data Collection

The RD_points are computed using
[libvmaf v0.2.1](https://github.com/Netflix/vmaf/tree/v2.2.1/libvmaf). For HDR
metrics, we are using HDRTools [v0.22 release branch](https://gitlab.com/standards/HDRTools/-/tree/0.22-dev)


| SL No | Metrics Name | SL No    | Metrics Name  |
| ----- | ------------ | -------- | ------------- |
| 1     | PSNR Y       | 9        | PSNR-HVS Cb   |
| 2     | PSNR Cb      | 10       | PSNR-HVS Cr   |
| 3     | PSNR Cr      | 11       | Encoding Time |
| 4     | PSNR-HVS     | 12       | Decoding Time |
| 5     | CIEDE2000    | 13       | VMAF          |
| 6     | SSIM         | 14       | VMAF-NEG      |
| 7     | MS-SSIM      | 15       | APSNR Y       |
| 8     | PSNR-HVS Y   | 16       | APSNR Cb      |
| 17    | APSNR Cr     | 18       | DE100         |
| 19    | PSNRL100     | 20       | wPSNR-Y       |
| 21    | wPSNR-U      | 22       | wPSNR-V       |
| 23    | HDR-VQM      |          |               |

The configuration files used to compute the metrics is adopted from the [3GPP
study](https://github.com/haudiobe/5GVideo/). This is available in the
[cfg](data/cfg/) folder of this material.

For ease of computation, we have also provided 3 scipts for computing them,
- HDR-VQM: `./HDR_VQM.sh $SRC-%05d.exr W H FPS NF $DST-%05d.exr`
- DeltaE+L100: `./HDRMetrics.sh $SRC-%05d.exr W H FPS NF $DST-%05d.exr`
- wPSNR: `./HDRMetrics_psnr.sh $SRC.yuv W H FPS NF $DST.yuv`

Sample command: `./HDR_VQM.sh aom_cosmos_3840x2160_24_hdr10_1573-1749.y4m-src-%05d.exr 3840 2160 24 130 aom_cosmos_3840x2160_24_hdr10_1573-1749.y4m-compressed-%05d.exr`



## Reference

[1]: Vibhoothi, François Pitié, Angeliki Katsenou, Daniel Joseph Ringis, Yeping Su, Neil Birkbeck, Jessie Lin, Balu Adsumilli, and Anil Kokaram. "Direct optimisation of λ for HDR content adaptive transcoding in AV1." In Applications of Digital Image Processing XLV, vol. 12226, pp. 36-45. SPIE, 2022. https://doi.org/10.1117/12.2632272

[2]: SPIE 2022 Supplementary material https://gitlab.com/mindfreeze/spie2022

[3]: ICME 2023, https://doi.org/10.1109/ICME55011.2023.00285
